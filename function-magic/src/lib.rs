
// Step 1: Defile a trait which is basically a call forwarder for tuple types
pub trait Handler<Args> : Clone + 'static {
    type Output;
    fn call(&self, args:Args) -> Self::Output;
}

// Step 2: create a bunch of implementations for tuple types
// test implementations -- need to be macrofied
impl<Func, R, Arg1> Handler<(Arg1,)> for Func where
Func: Fn(Arg1)->R+ Clone + 'static, R:Clone {
    type Output =R;
    fn call(&self, (arg1,) : (Arg1,)) -> Self::Output {
        (self)(arg1)
    }
}

impl<Func, R, Arg1, Arg2> Handler<(Arg1, Arg2)> for Func where
    Func: Fn(Arg1, Arg2)->R + Clone + 'static,
R:Clone{
    type Output =R;
    fn call(&self, (arg1, arg2) : (Arg1, Arg2)) -> Self::Output {
        (self)(arg1, arg2)
    }
}

// step 3: Have the state be stored somewhere
type AnyMap = std::collections::HashMap<std::any::TypeId, Box<dyn std::any::Any>>;

// step 4: Define a trait which can extract the type from the storage
pub trait FromState: Sized{
    fn extract(state: &AnyMap) -> Result<Self, String>;
}

fn extract_map<T:Sized+Clone+'static>(state: &AnyMap) -> Result<T, String> {
    let type_id = std::any::TypeId::of::<T>();
    let v = if let Some(v) = state.get(&type_id) {
        v
    } else {
        return Err("No state".to_string());
    };
    let down_cast = v.downcast_ref::<T>().unwrap();
    Ok(down_cast.clone())
}

// step4: Implement the extractor for tuple types
impl<Arg1:FromState + 'static> FromState for (Arg1,) {
    fn extract(state: &AnyMap) -> Result<Self, String> {
        Ok(
            (Arg1::extract(state)?,)
        )
    }
}

impl<Arg1:FromState + 'static, Arg2:FromState + 'static> FromState for (Arg1,Arg2) {
    fn extract(state: &AnyMap) -> Result<Self, String> {
        Ok(
            (Arg1::extract(state)?, Arg2::extract(state)?,)
        )
    }
}

// step 5: Create function which will convert the function or closure to something that be stored
// and called later
fn create_executor<F, Args>(handler: F) -> Box<dyn Fn(&AnyMap)>
    where F:Handler<Args>,
    Args:FromState{
    Box::new(move|state| {
        let res = Args::extract(state).expect("Failed to extract");
        handler.call(res);
    })
}

impl FromState for u32{
    fn extract(state: &AnyMap) -> Result<Self, String> {
        extract_map::<u32>(state)
    }
}

impl FromState for f32{
    fn extract(state: &AnyMap) -> Result<Self, String> {
        extract_map::<f32>(state)
    }
}

#[cfg(test)]
mod tests {
    use crate::{AnyMap, create_executor, FromState};

    fn test_access(v1:u32, v2:f32) {
        assert_eq!(v1, 40u32);
        assert_eq!(v2, 40.0);
    }

    struct CustomTypeAccessor(u32);

    impl FromState for CustomTypeAccessor {
        fn extract(state: &AnyMap) -> Result<Self, String> {
            u32::extract(state).map(|r| Self(r))
        }
    }

    fn test_custom_type_access(v:CustomTypeAccessor) {
        assert_eq!(v.0, 40u32);
    }

    #[test]
    fn basic_glue_check() {
        let mut map = AnyMap::new();
        map.insert(std::any::TypeId::of::<u32>(), Box::new(40u32));
        map.insert(std::any::TypeId::of::<f32>(), Box::new(40.0f32));

        let executor = create_executor(test_access);
        (executor)(&map);

        let executor_custom = create_executor(test_custom_type_access);
        (executor_custom)(&map);

        let executor_closure = create_executor(|v:f32| {
           assert_eq!(v, 40.0f32);
        });
        (executor_closure)(&map);
    }
}
