 use std:: {
     future::Future,
     pin::Pin,
     sync::{Arc,Mutex},
     task::{Context,Poll, Waker},
     thread,
     time::Duration,
 };
 use futures:: {
     future::{BoxFuture, FutureExt},
     task::{waker_ref, ArcWake}
 };
 use std::sync::mpsc::{Receiver, sync_channel, SyncSender};

 pub struct TimerFuture {
     shared_state: Arc<Mutex<SharedState>>
 }

 struct SharedState {
     completed:bool,
     waker: Option<Waker>
 }

 impl Future for TimerFuture {
     type Output = ();

     fn poll(self:Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
         let mut shared_state = self.shared_state.lock().unwrap();
         if shared_state.completed {
             Poll::Ready(())
         } else {
             // Set waker so that the thread can wake up the current task
             // when the timer has completed, ensuring that the future is polled
             // again and sees that `completed = true`.
             //
             // It's tempting to do this once rather than repeatedly cloning
             // the waker each time. However, the `TimerFuture` can move between
             // tasks on the executor, which could cause a stale waker pointing
             // to the wrong task, preventing `TimerFuture` from waking up
             // correctly.
             //
             // N.B. it's possible to check for this using the `Waker::will_wake`
             // function, but we omit that here to keep things simple.
             shared_state.waker = Some(cx.waker().clone());
             Poll::Pending
         }
     }
 }

 impl TimerFuture {
     pub fn new(duration: Duration) -> Self {
        let shared_state = Arc::new(Mutex::new(SharedState {
            completed: false,
            waker:None
        }));

         let thread_shared_state = shared_state.clone();
         thread::spawn(move || {
             thread::sleep(duration);
             let mut shared_state = thread_shared_state.lock().unwrap();
             shared_state.completed = true;
             if let Some(waker) = shared_state.waker.take() {
                 waker.wake()
             }
         });
         TimerFuture {shared_state}
     }
 }


 struct Executor {
     ready_queue: Receiver<Arc<Task>>,
 }

 #[derive(Clone)]
 struct Spawner {
     task_sender: SyncSender<Arc<Task>>,
 }

 struct Task {
     // Mutex not required, but need to satisfy rust guarantees
    future: Mutex<Option<BoxFuture<'static, ()>>>,
     task_sender:SyncSender<Arc<Task>>
 }

 fn new_executor_and_spawner() -> (Executor, Spawner) {
     const MAX_QUEUED_TASKS: usize = 10_000;
     let (task_sender, ready_queue) = sync_channel(MAX_QUEUED_TASKS);
     (Executor {ready_queue}, Spawner{task_sender})
 }

 impl Spawner {
     fn spawn(&self, future: impl Future<Output=()> + 'static + Send) {
         let future = future.boxed();
         let task = Arc::new(Task {
             future: Mutex::new(Some(future)),
             task_sender: self.task_sender.clone(),
         });
         self.task_sender.send(task).expect("too many tasks queued");
     }
 }

 impl ArcWake for Task {
     fn wake_by_ref(arc_self: &Arc<Self>) {
         // Implement `wake` by sending this task back onto the task channel
         // so that it will be polled again by the executor.
         let cloned = arc_self.clone();
         arc_self.task_sender.send(cloned).expect("Too many tasks queued");
     }
 }

 impl Executor {
     fn run(&self) {
         while let Ok(task) = self.ready_queue.recv() {
            // Take the future, and if it has not yet completed (is still Some),
             // poll it in an attempt to complete it.
             let mut future_slot = task.future.lock().unwrap();
             if let Some(mut future) = future_slot.take() {
                 // Create a `LocalWaker` from the task itself
                 let waker = waker_ref(&task);
                 let context = &mut Context::from_waker(&*waker);
                 // `BoxFuture<T>` is a type alias for
                 // `Pin<Box<dyn Future<Output = T> + Send + 'static>>`.
                 // We can get a `Pin<&mut dyn Future + Send + 'static>`
                 // from it by calling the `Pin::as_mut` method.
                 if future.as_mut().poll(context).is_pending() {
                     // We're not done processing the future, so put it
                     // back in its task to be run again in the future.
                     *future_slot = Some(future);
                 }
             }

         }
     }
 }

fn main() {
    let (executor, spawner) = new_executor_and_spawner();

    spawner.spawn(async {
        println!("howdy!");
        TimerFuture::new(Duration::new(2, 0)).await;
        println!("Done!");
    });

    // Drop to signal executor that there a no more tasks
    drop(spawner);

    executor.run();
}
