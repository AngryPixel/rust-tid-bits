// Support:
// [x] Inspecting public members
// [x] Custom display names
// [x] Getting values
// [x] Mutating property state
// [x] Array
// [x] Maps
// [ ] Tuple
// [ ] Option
// [ ] Custom mutator logic
// [x] Abstract data type builder
// [x] Convert abstract data type into concrete type
// [ ] undo and redo ops (OwnedValue?)
// [x] Store type id with property list
// [x] Trait to replace v from value
// [ ] Separate enum for data type only
//
//  Target:
//  #[derive(Property)]
//  struct Foo {
//     #[property(display_name="....", mutator=custom_mutator)]
//     x: i32,
//     y: i32,
//  }
//

//NOTE: Separate Mut and NonMut types won't work as the lack of compile time specialization
// prevents us from code generating the correct calls.

use std::any::TypeId;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::vec;

#[derive(Debug)]
pub enum MutateError {
    InvalidType,
    ReadOnly,
    PropertyNotFound(&'static str),
}

pub type MutateResult<T> = Result<T, MutateError>;

pub trait PropertyValue {
    fn as_value(&self) -> Value<'_>;
    fn mutate(&mut self, value: Value) -> MutateResult<()>;
}

pub trait ArrayValue {
    fn size(&self) -> usize;
    fn at(&self, index: usize) -> Option<&dyn PropertyValue>;
    fn at_mut(&mut self, index: usize) -> Option<&mut dyn PropertyValue>;
}

pub fn set_array_value<'a, 'b>(
    dest: &'a mut dyn ArrayValue,
    src: &'b dyn ArrayValue,
) -> MutateResult<()> {
    for i in 0..src.size() {
        let target = dest.at_mut(i).unwrap();
        target.mutate(src.at(i).unwrap().as_value())?;
    }
    Ok(())
}

pub trait MapValue {
    fn size(&self) -> usize;
    fn retrieve(&self, key: &Value) -> Option<&dyn PropertyValue>;
    fn retrieve_mut(&mut self, key: &Value) -> Option<&mut dyn PropertyValue>;
    fn store(&mut self, key: &Value, value: Value) -> MutateResult<()>;
    fn keys_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_>;
    fn values_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_>;
    //TODO: Tuple support
    //fn kv_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_>;
}

struct ValueIterator<'a, V: PropertyValue + 'a, T: Iterator<Item = &'a V> + 'a> {
    iterator: T,
}

impl<'a, V: PropertyValue + 'a, T: Iterator<Item = &'a V> + 'a> Iterator
    for ValueIterator<'a, V, T>
{
    type Item = Value<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next().map(|v| v.as_value())
    }
}


pub trait TupleValue {
    fn size(&self) -> usize;
    fn at(&self, index:usize) -> Option<&dyn PropertyValue>;
    fn at_mut(&mut self, index:usize) -> Option<&mut dyn PropertyValue>;
}

pub enum Value<'a> {
    Bool(bool),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    F32(f32),
    F64(f64),
    Str(&'a str),
    Type(&'a dyn PropertyList),
    Array(&'a dyn ArrayValue),
    Map(&'a dyn MapValue),
    Tuple(&'a dyn TupleValue)
}

impl PropertyValue for bool {
    fn as_value(&self) -> Value<'_> {
        Value::Bool(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Bool(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl<'a> TryFrom<&'a Value<'a>> for bool {
    type Error = MutateError;

    fn try_from(value: &'a Value<'a>) -> Result<Self, Self::Error> {
        if let Value::Bool(v) = value {
            Ok(*v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for i32 {
    fn as_value(&self) -> Value<'_> {
        Value::I32(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::I32(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl<'a> TryFrom<&'a Value<'a>> for i32 {
    type Error = MutateError;

    fn try_from(value: &'a Value<'a>) -> Result<Self, Self::Error> {
        if let Value::I32(v) = value {
            Ok(*v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for u32 {
    fn as_value(&self) -> Value<'_> {
        Value::U32(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::U32(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for i64 {
    fn as_value(&self) -> Value<'_> {
        Value::I64(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::I64(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for u64 {
    fn as_value(&self) -> Value<'_> {
        Value::U64(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::U64(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for f32 {
    fn as_value(&self) -> Value<'_> {
        Value::F32(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::F32(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for f64 {
    fn as_value(&self) -> Value<'_> {
        Value::F64(*self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::F64(v) = value {
            *self = v;
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl PropertyValue for &str {
    fn as_value(&self) -> Value<'_> {
        Value::Str(self)
    }

    fn mutate(&mut self, _: Value) -> MutateResult<()> {
        Err(MutateError::ReadOnly)
    }
}

impl PropertyValue for String {
    fn as_value(&self) -> Value<'_> {
        Value::Str(self.as_str())
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Str(v) = value {
            self.clear();
            self.push_str(v);
            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl<'a, T: PropertyValue> ArrayValue for &'a [T] {
    fn size(&self) -> usize {
        self.len()
    }

    fn at(&self, index: usize) -> Option<&dyn PropertyValue> {
        self.get(index).map(|t| t as &dyn PropertyValue)
    }

    fn at_mut(&mut self, _: usize) -> Option<&mut dyn PropertyValue> {
        None
    }
}

impl<'a, T: PropertyValue> PropertyValue for &'a [T] {
    fn as_value(&self) -> Value<'_> {
        Value::Array(self)
    }

    fn mutate(&mut self, _: Value) -> MutateResult<()> {
        Err(MutateError::ReadOnly)
    }
}

impl<T: PropertyValue> ArrayValue for Vec<T> {
    fn size(&self) -> usize {
        self.len()
    }

    fn at(&self, index: usize) -> Option<&dyn PropertyValue> {
        self.get(index).map(|t| t as &dyn PropertyValue)
    }

    fn at_mut(&mut self, index: usize) -> Option<&mut dyn PropertyValue> {
        self.get_mut(index).map(|t| t as &mut dyn PropertyValue)
    }
}

impl<T: PropertyValue + Default> PropertyValue for Vec<T> {
    fn as_value(&self) -> Value<'_> {
        Value::Array(self)
    }

    fn mutate(&mut self, v: Value) -> MutateResult<()> {
        if let Value::Array(v) = v {
            self.clear();
            // TODO improve handling for cases where this needs to be re-added

            self.resize_with(v.size(), Default::default);
            set_array_value(self, v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl<
        K: Hash + for<'a> TryFrom<&'a Value<'a>, Error = MutateError> + Eq + PropertyValue,
        V: PropertyValue + for<'a> TryFrom<&'a Value<'a>, Error = MutateError>,
    > MapValue for HashMap<K, V>
{
    fn size(&self) -> usize {
        self.len()
    }

    fn retrieve(&self, key: &Value) -> Option<&dyn PropertyValue> {
        // TODO: this should be an error
        let k = if let Ok(k) = K::try_from(key) {
            k
        } else {
            return None;
        };

        self.get(&k).map(|v| v as &dyn PropertyValue)
    }

    fn retrieve_mut(&mut self, key: &Value) -> Option<&mut dyn PropertyValue> {
        let k = if let Ok(k) = K::try_from(key) {
            k
        } else {
            return None;
        };

        self.get_mut(&k).map(|v| v as &mut dyn PropertyValue)
    }

    fn store(&mut self, key: &Value, value: Value) -> MutateResult<()> {
        let k = K::try_from(key)?;
        let v = V::try_from(&value)?;

        self.insert(k, v);
        Ok(())
    }

    fn keys_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_> {
        Box::new(ValueIterator {
            iterator: self.keys(),
        })
    }

    fn values_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_> {
        Box::new(ValueIterator {
            iterator: self.values(),
        })
    }
}

impl<
        K: Hash + for<'a> TryFrom<&'a Value<'a>, Error = MutateError> + Eq + PropertyValue,
        V: PropertyValue + for<'a> TryFrom<&'a Value<'a>, Error = MutateError>,
    > PropertyValue for HashMap<K, V>
{
    fn as_value(&self) -> Value<'_> {
        Value::Map(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Map(v) = value {
            self.clear();
            for key in v.keys_iter() {
                let map_value = v.retrieve(&key).unwrap().as_value();
                self.store(&key, map_value)?;
            }

            Ok(())
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl TupleValue for () {
    fn size(&self) -> usize {
        0
    }

    fn at(&self, _: usize) -> Option<&dyn PropertyValue> {
        None
    }

    fn at_mut(&mut self, _: usize) -> Option<&mut dyn PropertyValue> {
        None
    }
}

impl<T:PropertyValue> TupleValue for (T,) {
    fn size(&self) -> usize {
        1
    }

    fn at(&self, index: usize) -> Option<&dyn PropertyValue> {
        match index {
            0 => Some(&self.0),
            _=> None,
        }
    }

    fn at_mut(&mut self, index: usize) -> Option<&mut dyn PropertyValue> {
        match index {
            0 => Some(&mut self.0),
            _=> None,
        }
    }
}

impl<T:PropertyValue, V:PropertyValue> TupleValue for (T,V) {
    fn size(&self) -> usize {
        2
    }

    fn at(&self, index: usize) -> Option<&dyn PropertyValue> {
        match index {
            0 => Some(&self.0),
            1 => Some(&self.1),
            _=> None,
        }
    }

    fn at_mut(&mut self, index: usize) -> Option<&mut dyn PropertyValue> {
        match index {
            0 => Some(&mut self.0),
            1 => Some(&mut self.1),
            _=> None,
        }
    }
}

fn mutate_tuple(dst:&mut dyn TupleValue, src:&dyn TupleValue) -> MutateResult<()> {
    if dst.size() != src.size() {
        return Err(MutateError::InvalidType);
    }

    for i in 0..src.size() {
        dst.at_mut(i).unwrap().mutate(src.at(i).unwrap().as_value())?;
    }

    Ok(())
}

impl PropertyValue for () {
    fn as_value(&self) -> Value<'_> {
        Value::Tuple(self)
    }

    fn mutate(&mut self, _: Value) -> MutateResult<()> {
        Err(MutateError::ReadOnly)
    }
}


impl<T:PropertyValue> PropertyValue for (T,) {
    fn as_value(&self) -> Value<'_> {
        Value::Tuple(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Tuple(v) = value {
            mutate_tuple(self, v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl<T:PropertyValue, V:PropertyValue> PropertyValue for (T,V) {
    fn as_value(&self) -> Value<'_> {
        Value::Tuple(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Tuple(v) = value {
            mutate_tuple(self, v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Property {
    name: &'static str,
    index: usize,
}

pub trait PropertyList {
    fn type_id(&self) -> Option<TypeId> {
        None
    }
    fn property_count(&self) -> usize;
    fn get_property(&self, name: &str) -> Option<Property>;
    fn get_property_value_by_name(&self, name: &str) -> Option<&dyn PropertyValue> {
        self.get_property_value(self.get_property(name)?)
    }

    fn get_property_value(&self, property: Property) -> Option<&dyn PropertyValue>;
    fn get_property_value_mut(&mut self, property: Property) -> Option<&mut dyn PropertyValue>;
    fn get_properties(&self) -> &[Property];
}

fn set_from_property_list(
    target: &mut dyn PropertyList,
    source: &dyn PropertyList,
) -> MutateResult<()> {
    for property in source.get_properties() {
        if let Some(prop) = target.get_property(property.name) {
            target
                .get_property_value_mut(prop)
                .unwrap()
                .mutate(source.get_property_value(*property).unwrap().as_value())?;
        } else {
            return Err(MutateError::PropertyNotFound(property.name));
        }
    }

    Ok(())
}

impl<T: PropertyList> PropertyValue for T {
    fn as_value(&self) -> Value<'_> {
        Value::Type(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Type(v) = value {
            set_from_property_list(self, v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

pub enum OwnedValue {
    Bool(bool),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    F32(f32),
    F64(f64),
    Str(String),
    Type(OwnedType),
    Array(Vec<OwnedValue>),
}

/// Represents valid key value types
#[derive(Debug, Eq, PartialEq, Hash)]
pub enum OwnedKeyValue {
    Bool(bool),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    Str(String),
}

impl PropertyValue for OwnedKeyValue {
    fn as_value(&self) -> Value<'_> {
        match self {
            OwnedKeyValue::Bool(v) => v.as_value(),
            OwnedKeyValue::I32(v) => v.as_value(),
            OwnedKeyValue::U32(v) => v.as_value(),
            OwnedKeyValue::I64(v) => v.as_value(),
            OwnedKeyValue::U64(v) => v.as_value(),
            OwnedKeyValue::Str(v) => v.as_value(),
        }
    }

    fn mutate(&mut self, _: Value) -> MutateResult<()> {
        todo!()
    }
}

impl<'a> TryFrom<&'a Value<'a>> for OwnedKeyValue {
    type Error = MutateError;

    fn try_from(value: &'a Value<'a>) -> Result<Self, Self::Error> {
        match value {
            Value::Bool(v) => Ok(OwnedKeyValue::Bool(*v)),
            Value::I32(v) => Ok(OwnedKeyValue::I32(*v)),
            Value::U32(v) => Ok(OwnedKeyValue::U32(*v)),
            Value::I64(v) => Ok(OwnedKeyValue::I64(*v)),
            Value::U64(v) => Ok(OwnedKeyValue::U64(*v)),
            Value::Str(v) => Ok(OwnedKeyValue::Str(String::from(*v))),
            _ => Err(MutateError::InvalidType),
        }
    }
}

impl<'a> From<Value<'a>> for OwnedValue {
    fn from(v: Value<'a>) -> Self {
        match v {
            Value::Bool(v) => OwnedValue::Bool(v),
            Value::I32(v) => OwnedValue::I32(v),
            Value::U32(v) => OwnedValue::U32(v),
            Value::I64(v) => OwnedValue::I64(v),
            Value::U64(v) => OwnedValue::U64(v),
            Value::F32(v) => OwnedValue::F32(v),
            Value::F64(v) => OwnedValue::F64(v),
            Value::Str(v) => OwnedValue::Str(String::from(v)),
            Value::Type(v) => {
                let mut t = OwnedType::new();
                set_from_property_list(&mut t, v).expect("Shouldn't panic");
                OwnedValue::Type(t)
            }
            Value::Array(v) => {
                let mut vec = Vec::with_capacity(v.size());
                set_array_value(&mut vec, v).expect("Failed to set array value");
                OwnedValue::Array(vec)
            }
            Value::Map(_) => {
                todo!()
            }
            Value::Tuple(_) => {
                todo!()
            }
        }
    }
}

impl Default for OwnedValue {
    fn default() -> Self {
        OwnedValue::Bool(false)
    }
}

impl PropertyValue for OwnedValue {
    fn as_value(&self) -> Value<'_> {
        match self {
            OwnedValue::Bool(v) => v.as_value(),
            OwnedValue::I32(v) => v.as_value(),
            OwnedValue::U32(v) => v.as_value(),
            OwnedValue::I64(v) => v.as_value(),
            OwnedValue::U64(v) => v.as_value(),
            OwnedValue::F32(v) => v.as_value(),
            OwnedValue::F64(v) => v.as_value(),
            OwnedValue::Str(v) => v.as_value(),
            OwnedValue::Type(v) => v.as_value(),
            OwnedValue::Array(v) => v.as_value(),
        }
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        match self {
            OwnedValue::Bool(v) => v.mutate(value),
            OwnedValue::I32(v) => v.mutate(value),
            OwnedValue::U32(v) => v.mutate(value),
            OwnedValue::I64(v) => v.mutate(value),
            OwnedValue::U64(v) => v.mutate(value),
            OwnedValue::F32(v) => v.mutate(value),
            OwnedValue::F64(v) => v.mutate(value),
            OwnedValue::Str(v) => v.mutate(value),
            OwnedValue::Type(v) => v.mutate(value),
            OwnedValue::Array(v) => v.mutate(value),
        }
    }
}

fn new_from_property_list<T: Default + PropertyList>(
    v: &dyn PropertyList,
) -> Result<T, MutateError> {
    let mut new_value = T::default();
    set_from_property_list(&mut new_value, v)?;
    Ok(new_value)
}

pub struct OwnedType {
    properties: Vec<Property>,
    values: Vec<OwnedValue>,
}

impl OwnedType {
    pub fn new() -> Self {
        Self {
            properties: Vec::new(),
            values: Vec::new(),
        }
    }

    fn add_property(&mut self, name: &'static str, value: OwnedValue) {
        //TODO: validate names
        let prop_index = self.properties.len();
        self.properties.push(Property {
            name,
            index: prop_index,
        });
        self.values.push(value);
    }
}

impl PropertyList for OwnedType {
    fn property_count(&self) -> usize {
        self.properties.len()
    }

    fn get_property(&self, name: &str) -> Option<Property> {
        for prop in &self.properties {
            if prop.name == name {
                return Some(*prop);
            }
        }
        None
    }

    fn get_property_value(&self, property: Property) -> Option<&dyn PropertyValue> {
        self.values
            .get(property.index)
            .map(|v| v as &dyn PropertyValue)
    }

    fn get_property_value_mut(&mut self, property: Property) -> Option<&mut dyn PropertyValue> {
        self.values
            .get_mut(property.index)
            .map(|v| v as &mut dyn PropertyValue)
    }

    fn get_properties(&self) -> &[Property] {
        &self.properties
    }
}

pub struct OwnedMap {
    map: HashMap<OwnedKeyValue, OwnedValue>,
}

impl OwnedMap {
    pub fn new() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    fn insert(&mut self, key: &Value, value: Value) -> MutateResult<()> {
        let key = OwnedKeyValue::try_from(key)?;
        let value = OwnedValue::from(value);
        self.map.insert(key, value);
        Ok(())
    }
}

impl MapValue for OwnedMap {
    fn size(&self) -> usize {
        self.map.len()
    }

    fn retrieve(&self, key: &Value) -> Option<&dyn PropertyValue> {
        let key = if let Ok(k) = OwnedKeyValue::try_from(key) {
            k
        } else {
            return None;
        };
        self.map.get(&key).map(|v| v as &dyn PropertyValue)
    }

    fn retrieve_mut(&mut self, key: &Value) -> Option<&mut dyn PropertyValue> {
        let key = if let Ok(k) = OwnedKeyValue::try_from(key) {
            k
        } else {
            return None;
        };
        self.map.get_mut(&key).map(|v| v as &mut dyn PropertyValue)
    }

    fn store(&mut self, key: &Value, value: Value) -> MutateResult<()> {
        let key = OwnedKeyValue::try_from(key)?;
        let value = OwnedValue::from(value);
        self.map.insert(key, value);
        Ok(())
    }

    fn keys_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_> {
        Box::new(ValueIterator {
            iterator: self.map.keys(),
        })
    }

    fn values_iter(&self) -> Box<dyn Iterator<Item = Value<'_>> + '_> {
        Box::new(ValueIterator {
            iterator: self.map.values(),
        })
    }
}

impl PropertyValue for OwnedMap {
    fn as_value(&self) -> Value<'_> {
        Value::Map(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Map(map) = value {
            self.map.clear();
            for key in map.keys_iter() {
                let map_value = map.retrieve(&key).unwrap().as_value();
                self.store(&key, map_value)?;
            }
            Ok(())
        } else {
            return Err(MutateError::InvalidType);
        }
    }
}

pub struct OwnedTuple {
    data: Vec<OwnedValue>
}

impl OwnedTuple {
    pub fn new(size:usize) -> Self {
        let mut data = Vec::with_capacity(size);
        data.resize_with(size, Default::default);

        Self {
            data,
        }
    }

    pub fn set(&mut self, index:usize , value:Value) {
        self.data[index] = OwnedValue::from(value);
    }
}

impl PropertyValue for OwnedTuple {
    fn as_value(&self) -> Value<'_> {
        Value::Tuple(self)
    }

    fn mutate(&mut self, value: Value) -> MutateResult<()> {
        if let Value::Tuple(v) =value {
            mutate_tuple(self, v)
        } else {
            Err(MutateError::InvalidType)
        }
    }
}

impl TupleValue for OwnedTuple {
    fn size(&self) -> usize {
        self.data.len()
    }

    fn at(&self, index: usize) -> Option<&dyn PropertyValue> {
        self.data.get(index).map(|v| v as &dyn PropertyValue)
    }

    fn at_mut(&mut self, index: usize) -> Option<&mut dyn PropertyValue> {
        self.data.get_mut(index).map(|v| v as &mut dyn PropertyValue)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug, PartialEq)]
    struct Bar {
        float: f32,
        double: f64,
    }

    impl Bar {
        const PROPERTY_LIST: [Property; 2] = [
            Property {
                name: "float",
                index: 0,
            },
            Property {
                name: "double",
                index: 1,
            },
        ];
    }

    impl Default for Bar {
        fn default() -> Self {
            Self {
                float: 0.0,
                double: 0.0,
            }
        }
    }

    impl PropertyList for Bar {
        fn type_id(&self) -> Option<TypeId> {
            Some(TypeId::of::<Self>())
        }
        fn property_count(&self) -> usize {
            Self::PROPERTY_LIST.len()
        }

        fn get_property(&self, name: &str) -> Option<Property> {
            match name {
                "float" => Some(Self::PROPERTY_LIST[0]),
                "double" => Some(Self::PROPERTY_LIST[1]),
                _ => None,
            }
        }

        fn get_property_value(&self, property: Property) -> Option<&dyn PropertyValue> {
            match property.index {
                0 => Some(&self.float),
                1 => Some(&self.double),
                _ => None,
            }
        }

        fn get_property_value_mut(&mut self, property: Property) -> Option<&mut dyn PropertyValue> {
            match property.index {
                0 => Some(&mut self.float),
                1 => Some(&mut self.double),
                _ => None,
            }
        }

        fn get_properties(&self) -> &[Property] {
            &Self::PROPERTY_LIST
        }
    }

    #[derive(Debug, PartialEq)]
    struct Foo {
        is_true: bool,
        integer: i64,
        string: String,
        bar: Bar,
    }

    impl Default for Foo {
        fn default() -> Self {
            Self {
                is_true: false,
                integer: 0,
                string: String::new(),
                bar: Bar::default(),
            }
        }
    }

    impl Foo {
        const PROPERTY_LIST: [Property; 4] = [
            Property {
                name: "is_true",
                index: 0,
            },
            Property {
                name: "integer",
                index: 1,
            },
            Property {
                name: "string",
                index: 2,
            },
            Property {
                name: "bar",
                index: 3,
            },
        ];
    }

    impl PropertyList for Foo {
        fn type_id(&self) -> Option<TypeId> {
            Some(TypeId::of::<Self>())
        }

        fn property_count(&self) -> usize {
            Self::PROPERTY_LIST.len()
        }

        fn get_property(&self, name: &str) -> Option<Property> {
            match name {
                "is_true" => Some(Self::PROPERTY_LIST[0]),
                "integer" => Some(Self::PROPERTY_LIST[1]),
                "string" => Some(Self::PROPERTY_LIST[2]),
                "bar" => Some(Self::PROPERTY_LIST[3]),
                _ => None,
            }
        }

        fn get_property_value(&self, property: Property) -> Option<&dyn PropertyValue> {
            match property.index {
                0 => Some(&self.is_true),
                1 => Some(&self.integer),
                2 => Some(&self.string),
                3 => Some(&self.bar),
                _ => None,
            }
        }

        fn get_property_value_mut(&mut self, property: Property) -> Option<&mut dyn PropertyValue> {
            match property.index {
                0 => Some(&mut self.is_true),
                1 => Some(&mut self.integer),
                2 => Some(&mut self.string),
                3 => Some(&mut self.bar),
                _ => None,
            }
        }

        fn get_properties(&self) -> &[Property] {
            &Self::PROPERTY_LIST
        }
    }

    #[test]
    fn test_property_get() {
        let foo = Foo {
            is_true: false,
            integer: 1024,
            string: "Hello world".to_string(),
            bar: Bar {
                float: 0.2f32,
                double: 0.1,
            },
        };

        assert_eq!(foo.property_count(), 4);

        {
            let property = foo.get_property("is_true").unwrap();
            let value = foo.get_property_value(property).unwrap();
            assert!(matches!(value.as_value(), Value::Bool(false)));
        }

        {
            let property = foo.get_property("string").unwrap();
            let value = foo.get_property_value(property).unwrap();
            assert!(matches!(value.as_value(), Value::Str(_)));
        }

        {
            let property = foo.get_property("bar").unwrap();
            let value = foo.get_property_value(property).unwrap();
            if let Value::Type(t) = value.as_value() {
                assert_eq!(t.property_count(), 2);
                assert!(t.get_property("float").is_some());
                assert!(t.get_property("double").is_some());
            }
        }
    }

    #[test]
    fn test_property_mutation() {
        let mut foo = Foo {
            is_true: false,
            integer: 1024,
            string: "Hello world".to_string(),
            bar: Bar {
                float: 0.2f32,
                double: 0.1,
            },
        };

        {
            let property = foo.get_property("integer").unwrap();
            let value = foo.get_property_value_mut(property).unwrap();
            value
                .mutate(2048i64.as_value())
                .expect("Failed to set value");

            assert_eq!(foo.integer, 2048);
        }

        {
            let new_bar = Bar {
                float: 3.14,
                double: 1.5,
            };

            let property = foo.get_property("bar").unwrap();
            let value = foo.get_property_value_mut(property).unwrap();
            value
                .mutate(new_bar.as_value())
                .expect("Failed to set value");

            assert_eq!(foo.bar, new_bar);
        }
    }

    struct Array {
        vec: Vec<i32>,
    }

    impl Array {
        const PROPERTY_LIST: [Property; 1] = [Property {
            name: "vec",
            index: 0,
        }];
    }

    impl PropertyList for Array {
        fn type_id(&self) -> Option<TypeId> {
            Some(TypeId::of::<Self>())
        }
        fn property_count(&self) -> usize {
            Self::PROPERTY_LIST.len()
        }

        fn get_property(&self, name: &str) -> Option<Property> {
            match name {
                "vec" => Some(Self::PROPERTY_LIST[0]),
                _ => None,
            }
        }

        fn get_property_value(&self, property: Property) -> Option<&dyn PropertyValue> {
            match property.index {
                0 => Some(&self.vec),
                _ => None,
            }
        }

        fn get_property_value_mut(&mut self, property: Property) -> Option<&mut dyn PropertyValue> {
            match property.index {
                0 => Some(&mut self.vec),
                _ => None,
            }
        }

        fn get_properties(&self) -> &[Property] {
            &Self::PROPERTY_LIST
        }
    }

    #[test]
    fn test_array_value() {
        let array = Array { vec: vec![1, 2, 3] };

        let prop = array.get_property("vec").unwrap();
        let value = array.get_property_value(prop).unwrap();
        if let Value::Array(a) = value.as_value() {
            assert_eq!(a.size(), 3);
            for i in 0..a.size() {
                if let Value::I32(v) = a.at(i).unwrap().as_value() {
                    assert_eq!((i + 1) as i32, v);
                } else {
                    panic!("unexpected type")
                }
            }
        }
    }

    #[test]
    fn test_array_mutation() {
        let mut array = Array { vec: vec![1, 2, 3] };

        let prop = array.get_property("vec").unwrap();
        let value = array.get_property_value_mut(prop).unwrap();

        let new_values = [5i32, 6i32];

        value
            .mutate(new_values.as_slice().as_value())
            .expect("Failed to mutate");

        assert_eq!(array.vec.as_slice(), new_values.as_slice());
    }

    #[test]
    fn test_abstract_value() {
        let expected = Foo {
            is_true: false,
            integer: 9247,
            string: "My string".to_string(),
            bar: Bar {
                float: 3.14,
                double: 1024.5,
            },
        };

        let mut abstract_bar = OwnedType::new();
        let mut abstract_foo = OwnedType::new();

        abstract_bar.add_property("float", OwnedValue::F32(expected.bar.float));
        abstract_bar.add_property("double", OwnedValue::F64(expected.bar.double));
        abstract_foo.add_property("is_true", OwnedValue::Bool(expected.is_true));
        abstract_foo.add_property("integer", OwnedValue::I64(expected.integer));
        abstract_foo.add_property("string", OwnedValue::Str(expected.string.clone()));
        abstract_foo.add_property("bar", OwnedValue::Type(abstract_bar));

        let created =
            new_from_property_list::<Foo>(&abstract_foo).expect("Failed to create new instance");
        assert_eq!(expected, created);
    }

    #[test]
    fn test_abstract_array() {
        let mut vec: Vec<OwnedValue> = Vec::new();
        vec.push(1_i32.as_value().into());
        vec.push(2_i32.as_value().into());
        vec.push(3_i32.as_value().into());

        let expected = [1, 2, 3];

        let mut typed_vec = Vec::<i32>::new();

        typed_vec
            .mutate(vec.as_value())
            .expect("Failed to mutate vec");

        assert_eq!(typed_vec.as_slice(), &expected);
    }

    #[test]
    fn test_hashmap_value() {
        let mut map = HashMap::<i32, bool>::new();
        map.insert(20, true);
        map.insert(40, false);
        map.insert(50, true);

        let map_value: &dyn MapValue = &map;

        for k in map_value.keys_iter() {
            match k {
                Value::I32(v) => {
                    assert!(map.contains_key(&v));
                }
                _ => {
                    panic!("unexpected value")
                }
            }
        }
    }

    #[test]
    fn test_hashmap_value_mutate() {
        let map = {
            let mut map = HashMap::<i32, bool>::new();
            map.insert(20, true);
            map.insert(40, false);
            map.insert(50, true);
            map
        };

        let mut new_map = HashMap::<i32, bool>::new();

        let new_map_value: &mut dyn PropertyValue = &mut new_map;

        new_map_value
            .mutate(map.as_value())
            .expect("Failed to mutate");

        assert_eq!(new_map, map);
    }

    #[test]
    fn test_owned_map() {
        let expected = {
            let mut map = HashMap::<i32, bool>::new();
            map.insert(20, true);
            map.insert(40, false);
            map.insert(50, true);
            map
        };

        let mut owned_map = OwnedMap::new();
        owned_map
            .insert(&20.as_value(), true.as_value())
            .expect("Failed to insert value");
        owned_map
            .insert(&40.as_value(), false.as_value())
            .expect("Failed to insert value");
        owned_map
            .insert(&50.as_value(), true.as_value())
            .expect("Failed to insert value");

        let mut new_map = HashMap::<i32, bool>::new();
        new_map
            .mutate(owned_map.as_value())
            .expect("Failed to mutate map");

        assert_eq!(new_map, expected);
    }

    #[test]
    fn test_owned_tuple() {
        let expected = (1024, 3.14);
        let mut owned_tuple = OwnedTuple::new(2);
        owned_tuple.set(0,1024.as_value());
        owned_tuple.set(1, 3.14.as_value());

        let mut new_tuple = (0, 0.0);
        new_tuple.mutate(owned_tuple.as_value()).expect("failed to mutate");

        assert_eq!(expected, new_tuple);
    }
}


